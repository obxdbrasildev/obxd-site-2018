<?php include('templates/header-publicidade-e-marketing.php'); ?>



	       <!-- nossa cultura -->
	       <section id="nossa-cultura">
	       	 <article id="nossa-cultura" class="responsive">
	       	 	<h3>Como Funciona</h3>

	                <!-- nossa cultura box 1 -->
		       	 	<div id="nossa-cultura-box-1" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-1.png" class="nossa-cultura-icon" alt="missão">
		       	 		</figure>

		       	 		<h2 class="box-title">Colhemos as principais informações<br>do seu produto</h2>
		       	 		<br>
		       	 	</div>

		       	 	<!-- nossa cultura box 2 -->
		       	 	<div id="nossa-cultura-box-2" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-2.png" class="nossa-cultura-icon" alt="visão">
		       	 		</figure>

		       	 		<h2 class="box-title">Fazemos pesquisas e<br>analisamos concorrentes</h2>
		       	 		<br>
		       	 	</div>


                    <!-- nossa cultura box 3 -->
		       	 	<div id="nossa-cultura-box-3" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-3.png" class="nossa-cultura-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Produzimos o seu<br>projeto de Marketing</h2>
		       	 		<br>
					</div>
				
				</article>
	       </section>

	       <!-- nossa equipe -->
	       <section id="nossa-equipe">
	       	  <article id="nossa-equipe" class="responsive">
	       	  	 <h3>Serviços</h3>

	       	  	    <!-- nossa equipe box 1 -->
		       	 	<div id="nossa-equipe-box-1" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/pm-icon-1.png" class="jobs-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">SEO & SEM</h2>
						<br>
						<p>Obtenha resultaodos<br>precisos através de um<br>SEO e SEM bem<br>estruturados</p>
		       	 	</div>

		       	 	<!-- nossa equipe box 2 -->
		       	 	<div id="nossa-equipe-box-2" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/pm-icon-2.png" class="jobs-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">BI & Analytics</h2>
						<br>
						<p>Trace métricas e saiba<br>em tempo real o<br>andamento dos<br>resultados</p>
		       	 	</div>

		       	 	<!-- nossa equipe box 3 -->
		       	 	<div id="nossa-equipe-box-3" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/pm-icon-3.png" class="jobs-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Inbound Marketing</h2>
						<br>
						<p>Tenha uma lista<br>ilimitada de contatos<br>com potencias<br>clientes</p>
					</div>
						
					<!-- nossa equipe box 4 -->
		       	 	<div id="nossa-equipe-box-4" class="animated fadeInDown">
						<figure id="box">
						<img src="img/pm-icon-4.png" class="jobs-icon" alt="valores">
						</figure>

						<h2 class="box-title">Gestão de Redes Sociais</h2>
						<br>
						<p>Não tem tempo para<br>gerir suas redes sociais?<br>Nós damos aquela<br>força monstra</p>
					</div>

					<!-- nossa equipe box 5 -->
					<div id="nossa-equipe-box-5" class="animated fadeInDown">
						<figure id="box">
						<img src="img/pm-icon-5.png" class="jobs-icon" alt="valores">
						</figure>

						<h2 class="box-title">Content Marketing & Share</h2>
						<br>
						<p>Alcençe resultados esperados<br>com forte conteúdo do<br>seu projeto e divulgue<br>para seus clientes</p>
					</div>

					<!-- nossa equipe box 6 -->
					<div id="nossa-equipe-box-6" class="animated fadeInDown">
						<figure id="box">
						<img src="img/pm-icon-6.png" class="jobs-icon" alt="valores">
						</figure>

						<h2 class="box-title">Links Patrocinados</h2>
						<br>
						<p>Através do Google AdWords<br>é possível transformar<br>Links Patrocinados<br>em oportunidades</p>
					</div>

					<!-- nossa equipe box 7 -->
					<div id="nossa-equipe-box-7" class="animated fadeInDown">
						<figure id="box">
						<img src="img/pm-icon-7.png" class="jobs-icon" alt="valores">
						</figure>

						<h2 class="box-title">Comparadores de Preços</h2>
						<br>
						<p>Ideal para quem tem<br>loja virtual, comparadores<br>de preços ajuda na<br>conversão de resultados</p>
					</div>

					<!-- nossa equipe box 8 -->
					<div id="nossa-equipe-box-8" class="animated fadeInDown">
						<figure id="box">
						<img src="img/pm-icon-8.png" class="jobs-icon" alt="valores">
						</figure>

						<h2 class="box-title">Banners Google Display e Retargeting</h2>
						<br>
						<p>Saia na frente com<br>banners com design<br>otimizado para<br>arrebentar nas vendas</p>
					</div>

					<!-- nossa equipe box 9 -->
					<div id="nossa-equipe-box-9" class="animated fadeInDown">
						<figure id="box">
						<img src="img/pm-icon-9.png" class="jobs-icon" alt="valores">
						</figure>

						<h2 class="box-title">CRM & Partnership</h2>
						<br>
						<p>Fique na vanguarda<br>com um CRM de<br>alta qualidade</p>
					</div>

	       	  </article>
		   </section>
		   
		   <!-- demonstracao -->
		   <section id="pm-demonstration">
			   <article id="pm-demonstration" class="responsive">
					<h2>PUBLICIDADE INTELIGENTE<br>É UMA NOVA FORMA DE FAZER<br>MARKETING DE UM JEITO EFICIENTE</h2>
					<br>
					<p>Com a Publicidade Inteligente, é possível atingir os seus objetivos de vendas e mesmo<br> 
					   assim obter um alto retorno em seus negócios com o melhor do design otimizado que<br>
					   converte em vendas. Apareça para o mundo e venda muito mais!</p>

					<figure id="dm-icons">
						<img src="img/demonstracao.png" alt="demonstracao" class="demonstracao">
						<img src="img/facebook-dm.png" alt="demonstracao" class="facebook-dm">
						<img src="img/google-dm.png" alt="demonstracao" class="google-dm">
						<img src="img/instagram-dm.png" alt="demonstracao" class="instagram-dm">
						<img src="img/youtube-dm.png" alt="demonstracao" class="youtube-dm">
						<p class="dm-icons-text">Esse exemplo pode ser aplicado em<br>todas as peças e canais de mídia.</p>
					</figure>

                    <!-- dm-box-1 -->
					<div id="dm-box-1">
						<p>Empresas e Corporações<br><span class="big">100%</span><br>
						<p class="sec">satisfeitos</p>
					</div>

					<!-- dm-box-2 -->
					<div id="dm-box-2">
						<p>Pacotes Exclusivos com<br>todos os serviços digitais<br>de alto impacto</p>
					</div>

					<!-- dm-box-3 -->
					<div id="dm-box-3">
						<p>O melhor custo-benefício<br>do mercado de Marketing</p>
					</div>

					
			   </article>
		   </section>

<?php include('templates/footer.php'); ?>