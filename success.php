<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OBXD - Contato</title>
		<link href="css/success.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="js/jquery.cycle.all.js"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">

	</head>

    <!-- todo o site -->
	<body>
        <div class="animated fadeInDown">
            <h2 class="responsive">Sua mensagem foi enviada com sucesso! :)</h2>

            <a href="https://www.obxd.com.br" class="responsive">Voltar para o site</a>
        </div>
    </body>
</html>