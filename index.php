<?php include('templates/header.php'); ?>

	       <!-- informações -->
	       <section id="infos-categories">
	       	  <article id="infos-categories" class="responsive">

	       	  	 <div id="info-block-1">

	       	  	 	<h3 class="category-1">Design</h3>
	       	  	 	<br>
	       	  	 	<figure id="info-image">
	       	  	 		<img src="img/img1.jpg" class="info-image" alt="agencia-hacking">
	       	  	 	</figure>
	       	  	 	<br>
	       	  	 	  <div id="text-control">
			       	  	 	<h2>Tenha um design de alto padrão.</h2>
			       	  	 	<br>
			       	  	 	<p>Um site com forte apelo visual é fator decisivo para um sucesso de um negócio. Somos uma empresa especialista em deixar seu site da hora!</p>
			       	  	 	<br>
			       	  	 	<a href="design-de-interface.php" class="btn-ver-mais-block">Ver Mais</a>
			       	  	 	<p class="number">02</p>
		       	  	  </div>
	       	  	 </div>

	       	  	 <div id="info-block-2">

	       	  	 	<h3 class="category-2">Publicidade</h3>
	       	  	 	<br>
	       	  	 	<figure id="info-image">
	       	  	 		<img src="img/img2.jpg" class="info-image" alt="agencia-hacking">
	       	  	 	</figure>
	       	  	 	<br>
	       	  	 	  <div id="text-control">
			       	  	 	<h2>Arrebente nas vendas como nunca!</h2>
			       	  	 	<br>
			       	  	 	<p>Publicidade Inteligente é um novo conceito de publicidade focado mais no visual e alto retorno com investimentos expressivos.</p>
			       	  	 	<br>
			       	  	 	<a href="publicidade-e-marketing.php" class="btn-ver-mais-block">Ver Mais</a>
			       	  	 	<p class="number">03</p>
		       	  	  </div>
	       	  	 </div>

	       	  	 <div id="info-block-3">

	       	  	 	<h3 class="category-3">Sistemas</h3>
	       	  	 	<br>
	       	  	 	<figure id="info-image">
	       	  	 		<img src="img/img3.jpg" class="info-image" alt="agencia-hacking">
	       	  	 	</figure>
	       	  	 	<br>
	       	  	 	  <div id="text-control">
			       	  	 	<h2>Você tendo total controle do seu sistema.</h2>
			       	  	 	<br>
			       	  	 	<p>Tenha um sistema forte e robusto para aguentar os trancos do dia a dia sem ficar recorrendo a manutenções mensais burocráticas!</p>
			       	  	 	<br>
			       	  	 	<a href="sistemas-e-it.php" class="btn-ver-mais-block">Ver Mais</a>
			       	  	 	<p class="number">04</p>
		       	  	  </div>
	       	  	 </div>

	       	  </article>
	       </section>

<?php include('templates/footer.php'); ?>
	       