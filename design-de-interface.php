<?php include('templates/header-design-de-interface.php'); ?>


           <!-- nossa cultura -->
	       <section id="nossa-cultura">
	       	 <article id="nossa-cultura" class="responsive">
	       	 	<h3>Como Funciona</h3>

	                <!-- nossa cultura box 1 -->
		       	 	<div id="nossa-cultura-box-1" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-1.png" class="nossa-cultura-icon" alt="missão">
		       	 		</figure>

		       	 		<h2 class="box-title">Colhemos as principais informações<br>do seu produto</h2>
		       	 		<br>
		       	 	</div>

		       	 	<!-- nossa cultura box 2 -->
		       	 	<div id="nossa-cultura-box-2" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-2.png" class="nossa-cultura-icon" alt="visão">
		       	 		</figure>

		       	 		<h2 class="box-title">Fazemos pesquisas e<br>analisamos concorrentes</h2>
		       	 		<br>
		       	 	</div>


                    <!-- nossa cultura box 3 -->
		       	 	<div id="nossa-cultura-box-3" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-3.png" class="nossa-cultura-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Produzimos o seu<br>projeto de UX</h2>
		       	 		<br>
		       	 	</div>

	       	 </article>
	       </section>

	       <!-- nossa equipe -->
	       <section id="nossa-equipe">
	       	  <article id="nossa-equipe" class="responsive">
	       	  	 <h3>Aplicações</h3>

	       	  	    <!-- nossa equipe box 1 -->
		       	 	<div id="nossa-equipe-box-1" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/di-icon-4.png" class="jobs-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Web & Mobile</h2>
		       	 		<br>
		       	 	</div>

		       	 	<!-- nossa equipe box 2 -->
		       	 	<div id="nossa-equipe-box-2" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/di-icon-5.png" class="jobs-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Televisão</h2>
		       	 		<br>
		       	 	</div>

		       	 	<!-- nossa equipe box 3 -->
		       	 	<div id="nossa-equipe-box-3" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/di-icon-6.png" class="jobs-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Out Of Home</h2>
		       	 		<br>
		       	 	</div>

	       	  </article>
	       </section>
	      

<?php include('templates/footer.php'); ?>