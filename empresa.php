<?php include('templates/header-empresa.php'); ?>

	        <!-- nossa cultura -->
	       <section id="nossa-cultura">
	       	 <article id="nossa-cultura" class="responsive">
	       	 	<h3>Nossa Cultura</h3>

	                <!-- nossa cultura box 1 -->
		       	 	<div id="nossa-cultura-box-1" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/eye.png" class="nossa-cultura-icon" alt="missão">
		       	 		</figure>

		       	 		<h2 class="box-title">Missão</h2>
		       	 		<br>
		       	 		<p>Ser uma empresa que traga a <br>melhor experiência para todos.</p>
		       	 	</div>

		       	 	<!-- nossa cultura box 2 -->
		       	 	<div id="nossa-cultura-box-2" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/target.png" class="nossa-cultura-icon" alt="visão">
		       	 		</figure>

		       	 		<h2 class="box-title">Visão</h2>
		       	 		<br>
		       	 		<p>Ser uma empresa que influencia<br>e traga tendências para o mercado.</p>
		       	 	</div>


                    <!-- nossa cultura box 3 -->
		       	 	<div id="nossa-cultura-box-3" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/partner.png" class="nossa-cultura-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Valores</h2>
		       	 		<br>
		       	 		<p>Ética, transparência, sinceridade<br>e verdade para a tomada de decisão.</p>
		       	 	</div>

	       	 </article>
	       </section>

	       <!-- nossa equipe -->
	       <section id="nossa-equipe">
	       	  <article id="nossa-equipe" class="responsive">
	       	  	 <h3>Nossa Equipe</h3>

	       	  	    <!-- nossa equipe box 1 -->
		       	 	<div id="nossa-equipe-box-1" class="animated fadeInDown">
		       	 		<figure id="box">
		       	 			<img src="img/empresa-img-1.png" alt="Emerson">
		       	 		</figure>

		       	 		<h2 class="box-title">Emerson Garcia</h2>
		       	 		<br>
		       	 		<p>CEO & Chief Interactive Design</p>
		       	 	</div>

		       	 	<!-- nossa equipe box 2 -->
		       	 	<div id="nossa-equipe-box-2" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/empresa-img-2.png" alt="Marcelo">
		       	 		</figure>

		       	 		<h2 class="box-title">Marcelo Galdino</h2>
		       	 		<br>
		       	 		<p>Chief Developer & Technology</p>
		       	 	</div>

		       	 	<!-- nossa equipe box 3 -->
		       	 	<div id="nossa-equipe-box-3" class="animated fadeInDown">
		       	 		<figure id="box">
							<img src="img/empresa-img-3.png" alt="Amanda">
		       	 		</figure>

		       	 		<h2 class="box-title">Robson Mahé</h2>
		       	 		<br>
		       	 		<p>CDO (Chief Digital Officer)</p>
		       	 	</div>

	       	  </article>
	       </section>

<?php include('templates/footer.php'); ?>	      