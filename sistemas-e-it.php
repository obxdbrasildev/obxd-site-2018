<?php include('templates/header-sistemas.php'); ?>



	      <!-- nossa cultura -->
	       <section id="nossa-cultura">
	       	 <article id="nossa-cultura" class="responsive">
	       	 	<h3>Como Funciona</h3>

	                <!-- nossa cultura box 1 -->
		       	 	<div id="nossa-cultura-box-1" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-1.png" class="nossa-cultura-icon" alt="missão">
		       	 		</figure>

		       	 		<h2 class="box-title">Colhemos as principais informações<br>do seu produto</h2>
		       	 		<br>
		       	 	</div>

		       	 	<!-- nossa cultura box 2 -->
		       	 	<div id="nossa-cultura-box-2" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-2.png" class="nossa-cultura-icon" alt="visão">
		       	 		</figure>

		       	 		<h2 class="box-title">Fazemos pesquisas e<br>analisamos concorrentes</h2>
		       	 		<br>
		       	 	</div>


                    <!-- nossa cultura box 3 -->
		       	 	<div id="nossa-cultura-box-3" class="animated fadeInDown">
		       	 		<figure>
		       	 			<img src="img/di-icon-3.png" class="nossa-cultura-icon" alt="valores">
		       	 		</figure>

		       	 		<h2 class="box-title">Produzimos o seu<br>projeto do sistema</h2>
		       	 		<br>
		       	 	</div>

	       	 </article>
	       </section>

	       <!-- nossa equipe -->
	       <section id="nossa-equipe">
	       	  <article id="nossa-equipe" class="responsive">
	       	  	 <h3>Soluções</h3>

	       	  	    <!-- nossa equipe box 1 -->
		       	 	<div id="nossa-equipe-box-1" class="animated fadeInDown">
		       	 		<figure id="box">
                            <img src="img/sm-icon-1.png" class="jobs-icon" alt="valores">
                        </figure>

		       	 		<h2 class="box-title">Desenvolvimento de<br>Sites e Portais</h2>
		       	 		<br>
                        <p>Tenha um site moderno<br>e arrojado para divulgar<br>sua empresa</p>
		       	 	</div>

		       	 	<!-- nossa equipe box 2 -->
		       	 	<div id="nossa-equipe-box-2" class="animated fadeInDown">
		       	 		<figure id="box">
                            <img src="img/sm-icon-2.png" class="jobs-icon" alt="valores">
                        </figure>

		       	 		<h2 class="box-title">Desenvolvimento de<br>E-commerce</h2>
                        <br>
                        <p>Venda mais através<br>de uma loja virtual<br>robusta e com design<br>fortemente apelativo</p>
		       	 	</div>

		       	 	<!-- nossa equipe box 3 -->
		       	 	<div id="nossa-equipe-box-3" class="animated fadeInDown">
		       	 		<figure id="box">
                            <img src="img/sm-icon-3.png" class="jobs-icon" alt="valores">
                        </figure>

		       	 		<h2 class="box-title">Desenvolvimento de<br>Aplicativos</h2>
		       	 		<br>
                        <p>Destaque entre os demais<br>com aplicativos a frente<br>do seu tempo</p>
                    </div>
                        

                    <!-- nossa equipe box 4 -->
		       	 	<div id="nossa-equipe-box-4" class="animated fadeInDown">
                        <figure id="box">
                        <img src="img/sm-icon-4.png" class="jobs-icon" alt="valores">
                        </figure>

                        <h2 class="box-title">Desenvolvimento de<br>Sistemas Personalizados</h2>
                        <br>
                        <p>Faça a diferença no seu<br>negócio com um sistema<br>que atende a sua necessidade</p>
                    </div>

                    <!-- nossa equipe box 5 -->
		       	 	<div id="nossa-equipe-box-5" class="animated fadeInDown">
                        <figure id="box">
                        <img src="img/sm-icon-5.png" class="jobs-icon" alt="valores">
                        </figure>

                        <h2 class="box-title">Soluções em<br>Cloud</h2>
                        <br>
                        <p>Temos parceria com<br>as melhores empresas<br>de soluções cloud para<br>sua empresa</p>
                    </div>

                    <!-- nossa equipe box 6 -->
		       	 	<div id="nossa-equipe-box-6" class="animated fadeInDown">
                        <figure id="box">
                        <img src="img/sm-icon-6.png" class="jobs-icon" alt="valores">
                        </figure>

                        <h2 class="box-title">Aplicativos para<br>SmartWatches</h2>
                        <br>
                        <p>Saia na frente com<br>aplicativos de design<br>moderno para seu<br>relógio inteligente</p>
                    </div>

	       	  </article>
	       </section>

	       
<?php include('templates/footer.php'); ?>