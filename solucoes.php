<?php include('templates/header-solucoes.php'); ?>

           <!-- solução 1 -->
	       <section id="solucao-area-1">
              <article id="solucao-area-1" class="responsive">

                  <div id="solucao-selected-1">

                        <div id="control-top" class="animated fadeInDown">

                            <figure>
                                <img src="img/mao-celular.png" alt="Design de Interface e UX" class="solution-img">
                            </figure>

                            <h3>Design de Interface e UX</h3>
                            <br>
                            <p>
                                Desenvolvemos design de interface para diversos projetos web e TV, focados em experiências do usuário comum. Temos em comum apenas em se preocupar
								com as necessidades das pessoas e ouví-las sobre os projetos. Para isso fazemos pesquisas, entrevistas com usuários de diversos segmentos e colhemos informações.
								Entre os principais destaques estão criações de interfaces para sites, e-commerce, mobile e TV.
							</p>
							
							<a href="design-de-interface.php" class="btn-ver-mais-block">Ver Mais</a>
                            
                        </div>  

                  </div>

              </article>
           </section>

           <!-- solução 2 -->
	       <section id="solucao-area-2">
                <article id="solucao-area-2" class="responsive">
  
                    <div id="solucao-selected-2">
  
                          <div id="control-top" class="animated	fadeInDown">
  
                              <figure>
                                  <img src="img/televisao.png" alt="Design de Interface e UX" class="solution-img">
                              </figure>
  
                              <h3>Publicidade & Marketing Digital</h3>
                              <br>
                              <p>
								Uma nova forma de publicidade e anúncios na web focado em design otimizado para vendas online, utilizando os principais canais de mídia online, atingindo
								o maior número de pessoas possível. Uma nova era da publicidade e marketing está começando agora! Publicidade inteligente focada em gerar resultados incríveis
								por meio do design otimizado focado em rápida conversão.
							  </p>
							  
							  <a href="publicidade-e-marketing.php" class="btn-ver-mais-block">Ver Mais</a>
                              
                          </div>  
  
                    </div>
  
                </article>
           </section>


           <!-- solução 3 -->
	       <section id="solucao-area-3">
                <article id="solucao-area-3" class="responsive">
  
                    <div id="solucao-selected-3">
  
                          <div id="control-top" class="animated	fadeInDown">
  
                              <figure>
                                  <img src="img/servers.png" alt="Design de Interface e UX" class="solution-img">
                              </figure>
  
                              <h3>Desenvolvimento De Sistemas</h3>
                              <br>
                              <p>
								  Desenvolvemos sistemas completos e robustos que atendem a necessidade da sua empresa
								  ou negócio. Nossos sistemas são desenvolvidos com as maiores práticas de desenvolvimento
								  que obedecem as principais normas de segurança da informação e proteção de dados.
							  </p>
							  
							  <a href="sistemas-e-it.php" class="btn-ver-mais-block">Ver Mais</a>
                              
                          </div>  
  
                    </div>
  
                </article>
           </section>

<?php include('templates/footer.php'); ?>